---
id: order
title: order
---

Allows you to interact with order and related endpoints.

## all

`[GET] /api/orders`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## create

`[POST] /api/orders`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## new

`[GET] /api/orders/new`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## edit

`[GET] /api/orders/:number/edit`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/orders/:number`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/orders/:number`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/orders/:number`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## mine

`[GET] /api/orders/mine`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## current

`[GET] /api/orders/current`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## cancel

`[PUT] /api/orders/:number/cancel`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## empty

`[PUT] /api/orders/:number/empty`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## create

`[POST] /api/orders/:order.number/coupon_codes`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/orders/:order.number/coupon_codes/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## all

`[GET] /api/orders/:order.number/line_items`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## create

`[POST] /api/orders/:order.number/line_items`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## new

`[GET] /api/orders/:order.number/line_items/new`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/orders/:order.number/line_items/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## edit

`[GET] /api/orders/:order.number/line_items/:id/edit`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/orders/:order.number/line_items/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/orders/:order.number/line_items/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## all

`[GET] /api/orders/:order.number/payments`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## create

`[POST] /api/orders/:order.number/payments`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## new

`[GET] /api/orders/:order.number/payments/new`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## edit

`[GET] /api/orders/:order.number/payments/:id/edit`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/orders/:order.number/payments/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/orders/:order.number/payments/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/orders/:order.number/payments/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## get

`[GET] /api/orders/:order.number/addresses/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/orders/:order.number/addresses/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## all

`[GET] /api/orders/:order.number/return_authorizations`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## create

`[POST] /api/orders/:order.number/return_authorizations`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## new

`[GET] /api/orders/:order.number/return_authorizations/new`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## edit

`[GET] /api/orders/:order.number/return_authorizations/:id/edit`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## cancel

`[PUT] /api/orders/:order.number/return_authorizations/:id/cancel`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## update

`[PATCH] /api/orders/:order.number/return_authorizations/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)

## delete

`[DELETE] /api/orders/:order.number/return_authorizations/:id`

> **🚨 This endpoint is missing documentation.**
>
> Help out by opening a [merge request](https://gitlab.com/deseretbook/packages/solidus-sdk), and
be sure to follow the instructions found in the [Contributing Guide](../contributing.md#2-update-documentation).
>
> Here are some helpful links that should help you get started:
>
> - [Solidus Developers Guide](https://guides.solidus.io/developers/index.html)
> - [Solidus API Reference](https://solidus.docs.stoplight.io/)
> - [Solidus API Source](https://github.com/solidusio/solidus/tree/master/api/app/controllers/spree/api)
> - [Solidus API Request Specs](https://github.com/solidusio/solidus/tree/master/api/spec/requests/spree/api)