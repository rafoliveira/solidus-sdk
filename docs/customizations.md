---
id: customizations
title: Customizations
sidebar_label: Customizations
---

## Built with `sdk-factory`

At its core `solidus-sdk` is built using the [`sdk-factory` package](https://www.npmjs.com/package/sdk-factory)

This means any options available to the `sdk-factory` are available to the `solidus-sdk`.
