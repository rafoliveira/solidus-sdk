---
id: configuration
title: Configuration
sidebar_label: Configuration
---

`solidus-sdk` provides a class that can be initialized with your customizations and other settings
needed to interact with your instance of the Solidus API. The following are some of the basic
configuration options available to you. See the [section about Customizations](customizations.md)
for more advanced options available to you.

## Example

``` javascript
import { Solidus } from 'solidus-sdk'

// Optional plugins
import reviewsPlugin from 'solidus-plugin-reviews'

// Initialize sdk with configurations
const sdk = new Solidus({
  href: 'https://mysolidus.app', // required
  apiKey: SOLIDUS_API_KEY,
  plugins: [
    reviewsPlugin,
  ],
})
```

## Options

### href [string] (required)

The base path to the Solidus API.

### apiKey [string]

API key to include with every request. This adds the proper authorization
headers.

### headers [object]

Additional headers that should be added with every request.

### plugins [array]

Solidus plugins that modify and add additional endpoint support to the base API provided by the
`solidus-sdk` package. See the [section about Plugins](plugins.md) for further details on what this
is for and how to use it.

