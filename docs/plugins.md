---
id: plugins
title: Plugins
sidebar_label: Plugins
---

One of the benefits of using Solidus is the amount of [extensions available](https://solidus.io/extensions/).

Many of these extensions provide API endpoints that enhance or modify the vanilla Solidus API. In
order to support these extensions, the `solidus-sdk` provides first class support for these extensions
through **plugins**.

## Installing a Plugin

In order for you to add a plugin to your instance of the solidus-sdk, just follow these steps:

1. Install the plugin, eg:

```bash
yarn add solidus-plugin-reviews
```

2. Include the plugin in your sdk settings, eg:

```js
import { Solidus } from 'solidus-sdk'
import reviews from 'solidus-plugin-reviews'

const sdk = new Solidus({
  href: 'https://mysolidus.app',
  plugins: [reviews],
})
```

3. That's it! Any endpoints / overrides from the plugins will automatically be added.


## Community Plugins

Below is the current list of plugins available. Know of a plugin that's not on the list? [Open a merge request](https://gitlab.com/deseretbook/packages/solidus-sdk) to add it!

### [solidus-plugin-reviews](https://www.npmjs.com/package/solidus-plugin-reviews)
Adds support for the [solidus_reviews](https://github.com/solidusio-contrib/solidus_reviews) gem.


## Writing a Plugin

If you know of an extension to Solidus that doesn't have a plugin yet, or if your team has custom
endpoints you need to support, you can write your own plugin.

A basic plugin is just an object with a `name` and `endpoints` properties, and will look something
like this:

``` javascript
const examplePlugin = {
  name: 'solidus-plugin-example',
  endpoints: {
    rewards: {
      endpoints: {
        all: {
          method: 'GET',
          path: '/api/rewards',
        },
      },
    },
  },
}
```

The above example would add support for accessing a `/api/rewards` endpoint. This will translate
in the sdk to:

``` javascript

import { Solidus } from 'solidus-sdk'
import examplePlugin from './path/to/examplePlugin'

const sdk = new Solidus({
  href: 'https://mysolidus.app',
  plugins: [examplePlugin],
})

sdk.rewards().all()
  .then(response => console.log('API response', response))

```


### Keep in Mind

Keep in mind when writing a plugin. Plugin endpoints are merged into the default Solidus schema,
so any collisions between schemas will override previous values in favor of newer ones.

This means that a plugin can override the existing behavior of the `solidus-sdk` as well as add to
it. It also means that plugins added later can override other plugins.

`solidus-sdk` is built using `sdk-factory`, so for further information,
[see the `sdk-factory` documentations](https://www.npmjs.com/package/sdk-factory).

When publishing a new plugin, it's encouraged you pre-pend the package name with `solidus-plugin-`
to make it easier to find and so we can keep things consistent within the community.
