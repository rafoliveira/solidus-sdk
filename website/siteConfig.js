/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// See https://docusaurus.io/docs/site-config for all the possible
// site configuration options.

const siteConfig = {
  title: 'solidus-sdk', // Title for your website.
  tagline: 'A JavaScript SDK for interacting with a Solidus E-commerce API',
  url: 'https://solidus-sdk.netlify.com', // Your website URL
  baseUrl: '/', // Base URL for your project */
  // For github.io type URLs, you would set the url and baseUrl like:
  //   url: 'https://facebook.github.io',
  //   baseUrl: '/test-site/',

  // Used for publishing and more
  projectName: 'solidus-sdk',
  organizationName: 'deseretbook',
  // For top-level user or org sites, the organization is still the same.
  // e.g., for the https://JoelMarcey.github.io site, it would be set like...
  //   organizationName: 'JoelMarcey'

  // For no header links in the top nav bar -> headerLinks: [],
  headerLinks: [
    { doc: 'basic', label: 'Docs' },
    {
      href: 'https://gitlab.com/deseretbook/packages/solidus-sdk',
      label: 'GitLab',
    },
    {
      href: 'https://solidus.io/',
      label: 'Solidus',
    },
  ],

  /* path to images for header/footer */
  headerIcon: 'images/logo.png',
  footerIcon: 'images/logo.png',
  favicon: 'images/favicon.ico',

  /* Colors for website */
  colors: {
    primaryColor: '#6c63ff',
    secondaryColor: '#564fcc',
  },

  highlight: {
    // Highlight.js theme to use for syntax highlighting in code blocks.
    theme: 'default',
  },

  // On page navigation for the current documentation page.
  onPageNav: 'separate',

  // No .html extensions for paths.
  cleanUrl: true,

  editUrl:
    'https://gitlab.com/deseretbook/packages/solidus-sdk/tree/master/docs/',

  // For sites with a sizable amount of content, set collapsible to true.
  // Expand/collapse the links and subcategories under categories.
  docsSideNavCollapsible: true,

  // Show documentation's last contributor's name.
  // enableUpdateBy: true,

  // Show documentation's last update time.
  // enableUpdateTime: true,

  // You may provide arbitrary config keys to be used as needed by your
  // template. For example, if you need your repo's URL...
  repoUrl: 'https://gitlab.com/deseretbook/solidus-sdk',
  solidusUrl: 'https://solidus.io/',
}

module.exports = siteConfig
